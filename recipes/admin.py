from django.contrib import admin

# Register your models here.
from .models import Recipe
from .models import RecipeStep
from .models import Ingredient


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = ("title", "description", "created_on", "id")



@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = ("step_number", "instruction", "id")



@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = ("amount", "food_item")
